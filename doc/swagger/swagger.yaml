swagger: '2.0'

info:
  version: "0.1.0"
  title: "BlattWerkzeug"

  description: BlattWerkzeug is a data orientated IDE for learners in schools and elsewhere.

  contact:
    name: Marcus Riemer

    email: mri@fh-wedel.de

  license:
    name: GNU Affero General Public License v3
    url: http://www.gnu.org/licenses/agpl-3.0.en.html

host: "esqulino.marcusriemer.de"
basePath: /api
schemes:
  - https

# Describe your paths here
paths:
  /project:
    get:
      operationId : getAvailableProjects
      tags: [project]
      description: Retrieves all publicly available projects
      produces:
        - application/json
      responses:
        200:
          description: Successful response
          schema:
            type: array
            title: ArrayOfProjects
            items:
              "$ref": "./ProjectListDescription.json"
  /project/{id}:
    get:
      operationId : getProjectById
      tags: [project]
      description: Retrieves a specific project with all available information. As this includes the whole syntax-tree for every query, this description is quite exhaustive.
      parameters:
        - name: id
          in: path
          description: Id of the project to fetch
          required: true
          type: string
      responses:
        200:
          description: Succesful retrieval of a project.
          schema:
            "$ref": "./ProjectDescription.json"
    post:
      operationId : updateProjectById
      tags: [project]
      description: Updates properties of the project itself, but does not change any query or page. To change any of those entities, call the matching `update` endpoints.
      parameters:
        - name: id
          in: path
          description: Id of the project to update
          required: true
          type: string
        - name: Project Information
          in: body
          description: Project description to save
          required: true
          schema:
            $ref: "./ProjectListDescription.json"
      responses:
        200:
          description: Project was updated
  /project/{id}/query/:
    post:
      operationId : createQuery
      tags: [query]
      description: Creating a query on the server.
      parameters:
        - name: id
          in: path
          description: Id of the project the query belongs to.
          required: true
          type: string
        - name: Query
          in: body
          description: The syntax tree and SQL representation of the query. If this representation should have an ID assigned, it will be ignored.
          required: false
          schema:
            $ref: "./QueryUpdateRequestDescription.json"
      responses:
        200:
          description: The query has been created.
          schema:
            "$ref": "./QueryResultDescription.json"
  /project/{id}/query/{queryId}:
    post:
      operationId : updateQueryById
      tags: [query]
      description: Updating a query on the server.
      parameters:
        - name: id
          in: path
          description: Id of the project the query belongs to.
          required: true
          type: string
        - name: queryId
          in: path
          description: Id of the query to update.
          required: true
          type: string
          format: uuid
        - name: Query
          in: body
          description: The syntax tree and SQL representation of the query. If this representation should have an ID assigned, it will be ignored.
          required: false
          schema:
            $ref: "./QueryUpdateRequestDescription.json"
      responses:
        200:
          description: The query has been updated.
          schema:
            "$ref": "./QueryResultDescription.json"
    delete:
      operationId : deleteQueryById
      tags: [query]
      description: Deleting an existing query
      parameters:
        - name: id
          in: path
          description: Id of the project the query belongs to.
          required: true
          type: string
        - name: queryId
          in: path
          description: Id of the query to update. If this is left out a new query will be created.
          required: true
          type: string
          format: uuid
      responses:
        200:
          description: Succesful deletion of query

  /project/{id}/query/{queryId}/run:
    post:
      operationId : runQueryById
      tags: [query]
      description: Running a query that has already been stored on this server.
      parameters:
        - name: id
          in: path
          description: Id of the project the query belongs to
          required: true
          type: string
        - name: queryId
          in: path
          description: Id of the query to execute
          required: true
          type: string
          format: uuid
        - name: Query Parameters
          in: body
          description: If the query makes use of user defined values, these need to be transferred in the body.
          required: false
          schema:
            $ref: "./QueryParamsDescription.json"
      responses:
        200:
          description: Succesful execution of the query.
          schema:
            "$ref": "./QueryResultDescription.json"
        400:
          description: Something went wrong while executing the query

  /project/{id}/run:
    post:
      operationId : runArbitraryQuery
      tags: [query]
      description: Running an arbitrary query that is part of the body of the request. This is usually done during development with the integrated editor.
      parameters:
        - name: id
          in: path
          description: Id of the project the query belongs to. Without a project there would not be a database to run the query against.
          required: true
          type: string
        - name: Query Parameters
          in: body
          description: As the server does not have the SQL representation available, it needs to be part of the request. If the query makes use of user defined values, these need to be transferred in the body.
          required: true
          schema:
            $ref: "./ArbitraryQueryRequestDescription.json"
      responses:
        200:
          description: Succesful execution of the query.
          schema:
            "$ref": "./QueryResultDescription.json"
        400:
          description: Something went wrong while executing the query
          schema:
            "$ref": "./RequestErrorDescription.json"
  /project/{id}/page/:
    post:
      operationId : createPage
      tags: [page]
      description: Creating a new page, expecting a ID back from the server.
      parameters:
        - name: id
          in: path
          description: Id of the project the page belongs to.
          required: true
          type: string
        - name: Page
          in: body
          description: The syntax tree and the renderable representation of the page. If this representation should have an ID assigned, it will be ignored.
          required: false
          schema:
            $ref: "./PageUpdateRequestDescription.json"
      responses:
        200:
          description: The page has been created, this response is a naked string with the ID of the newly created page.
          schema:
            type: string
  /project/{id}/page/{pageId}:
    post:
      operationId : updatePageById
      tags: [page]
      description: Updating a page on the server.
      parameters:
        - name: id
          in: path
          description: Id of the project the page belongs to.
          required: true
          type: string
        - name: pageId
          in: path
          description: Id of the page to update.
          required: true
          type: string
          format: uuid
        - name: Page
          in: body
          description: The syntax tree and the renderable representation of the page. If this representation should have an ID assigned, it will be ignored.
          required: false
          schema:
            $ref: "./PageUpdateRequestDescription.json"
      responses:
        200:
          description: The page has been updated.
    delete:
      operationId: deletePageById
      tags: [page]
      description: Deletes a page on the server.
      parameters:
        - name: id
          in: path
          description: Id of the project the page belongs to.
          required: true
          type: string
        - name: pageId
          in: path
          description: Id of the page to delete.
          required: true
          type: string
          format: uuid
      responses:
        200:
          description: "Page has been deleted"
          

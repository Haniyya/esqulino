import {Component}                      from '@angular/core'

@Component({
    selector: 'sql-scratch',
    template: `<router-outlet></router-outlet>`
})
export class SqlScratchComponent {

}

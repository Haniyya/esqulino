import {RequestErrorDescription}              from '../serverapi.service'

import {Model}                                from './base'
import {QuerySelect}                          from './select'

/**
 * Not much ado about type safety here, in the raw
 * format every cell is a string.
 */
type RawRow = [string]

/**
 * A result is simply a list of rows.
 */
type QueryResultDescription = RawRow[]

/**
 * Over the wire format to describe a query that could not
 * be run on the server.
 */
export interface QueryRunErrorDescription extends RequestErrorDescription {

}

/**
 * Provides some extra type information for a certain cell.
 */
class Cell {
    /**
     * The query this cell is a result of
     */
    private _query : QuerySelect;

    /**
     * The column index of this cell
     */
    private _index : number;

    /**
     * The value of this cell
     */
    private _value : string;

    /**
     * Constructs a concrete cell
     *
     * @param query The query this cell is a result of
     * @param index The column index of this cell
     * @param value The value of this cell
     */
    constructor(query : QuerySelect, index : number, value : string) {
        this._query = query;
        this._index = index;
        this._value = value;
    }

    /**
     * Possibly formats the value based on the type.
     */
    get value() {
        return (this._value);
    }
}

/**
 * Allows to adress columns by name or index.
 */
class Row {
    private _query : QuerySelect;
    private _cells : Cell[];
    
    constructor(query : QuerySelect, raw : RawRow) {
        this._query = query;
        this._cells = raw.map( (v,k) => new Cell(query, k, v));
    }

    get cells() {
        return (this._cells);
    }
}

function isQueryRunErrorDescription(arg : any) : arg is QueryRunErrorDescription {
    return (arg.message !== undefined);
}

function isQueryResultDescription(arg : any) : arg is QueryResultDescription {
    return (Array.isArray(arg));
}

/**
 * Adds type information to a raw QueryResultDescription.
 */
export class SelectQueryResult {
    private _query : QuerySelect;

    private _rows : Row[] = [];

    /**
     * If this field is set, the query was not succesfull
     */
    private _error : QueryRunErrorDescription;

    /**
     * A result may be an error or a list of rows.
     *
     * @param query The query that was running
     * @param res   The result of the run
     */
    constructor(query : QuerySelect, res : QueryResultDescription | QueryRunErrorDescription) {
        this._query = query;

        if (isQueryRunErrorDescription(res)) {
            this._error = res;
        } else if (isQueryResultDescription(res)) {
            this._rows = res.map( v => new Row(query, v));
        }
    }

    /**
     * @return True, if the promised row count matches the actual row count.
     */
    get hasValidSingleRowCount() {
        // If this query does not expect a single row everything goes, otherwise
        // the result must exactly be a single row!
        return (!this._query.singleRow || this._rows.length === 1);
    }

    /**
     * @return True, if this result is an error.
     */
    get isError() : boolean {
        return (!!this._error);
    }

    /**
     * @return The servers error message.
     */
    get errorMessage() {
        return (this._error.message);
    }

    /**
     * @return All result rows
     */
    get rows() {
        return (this._rows);
    }

    /**
     * @return The names of the columns involved in this result.
     */
    get cols() {
        return (this._query.select.actualColums);
    }
}

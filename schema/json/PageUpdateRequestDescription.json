{
    "description": "Storing a page on the server",
    "type": "object",
    "properties": {
        "model": {
            "$ref": "#/definitions/PageDescription",
            "description": "The backend model to store."
        },
        "sources": {
            "description": "Serialized representations to store.",
            "type": "object",
            "additionalProperties": {
                "type": "string"
            }
        }
    },
    "additionalProperties": false,
    "definitions": {
        "PageDescription": {
            "type": "object",
            "properties": {
                "body": {
                    "$ref": "#/definitions/BodyDescription",
                    "description": "The host for all widgets that are part of this page."
                },
                "referencedQueries": {
                    "description": "IDs of queries that are referenced in this page. Only\n\nthese queries provide additional DB information that can\n\nbe used on this page.",
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/QueryReferenceDescription",
                        "description": "Referenced queries are possibly accompanied by a human-readable\n\nname. This is required if the same query is going to be used\n\nmultiple times on a single page."
                    }
                },
                "parameters": {
                    "description": "All parameters that are required to render this page. These\n\nare usually satisfied via GET parameters",
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/PageParameterDescription",
                        "description": "A parameter that is required to render a page."
                    }
                },
                "id": {
                    "description": "An internal ID, possibly a GUID, to uniquely identify a resource.\n\nThese IDs must *never* change and should be UUIDs, so that they are\n\nglobally unique.",
                    "type": "string"
                },
                "name": {
                    "description": "The user-chosen name of this resource. This property is free to change.",
                    "type": "string"
                },
                "apiVersion": {
                    "$ref": "#/definitions/ApiVersionToken",
                    "description": "These API versions are merely a number, no need to overcomplicate things.\n\nIf versions differ, any implementing the API is free to specify\n\nversion ranges it works with."
                }
            },
            "additionalProperties": false
        },
        "BodyDescription": {
            "description": "The body of an HTML page, will only appear once in a page.",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [
                        "body"
                    ]
                },
                "children": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/WidgetDescription",
                        "description": "A widget **requires** at least a type, all other fields are\n\nmandated by deriving descriptions. As we don't necesarily\n\nknow all deriving classes at compile time (they could be\n\nprovided by a plugin) we poke a hole in the type system\n\nhere.\n\n\n\nThe following annotion is required to allow additional\n\nproperties in the automatically generated JSON schema, see\n\nhttps://github.com/YousefED/typescript-json-schema/issues/44",
                        "additionalProperties": true
                    }
                }
            },
            "additionalProperties": false
        },
        "WidgetDescription": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string"
                }
            },
            "additionalProperties": true
        },
        "QueryReferenceDescription": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [
                        "query"
                    ]
                },
                "queryId": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "mapping": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/ParameterMappingDescription",
                        "description": "Inputs and outputs *may* use identical names but are not required\n\nto do so."
                    }
                }
            },
            "additionalProperties": false
        },
        "ParameterMappingDescription": {
            "type": "object",
            "properties": {
                "parameterName": {
                    "type": "string"
                },
                "providingName": {
                    "type": "string"
                }
            },
            "additionalProperties": false
        },
        "PageParameterDescription": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                }
            },
            "additionalProperties": false
        },
        "ApiVersionToken": {
            "description": "It would be nicer if versions could be numbers, but Typescript has no\n\nconcept of union types for numbers.",
            "type": "string",
            "enum": [
                "1",
                "2",
                "3"
            ]
        }
    },
    "$schema": "http://json-schema.org/draft-04/schema#"
}
